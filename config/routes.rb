Rails.application.routes.draw do

  get '/phrases/translate', to: 'phrases#translate'
  resources :users_languages
  resources :users
  resources :repetitions
  resources :repetitions_phrases
  resources :phrases
  resources :languages
  resources :groups_trainees
  resources :groups_subgroups
  resources :groups_repetitions
  resources :groups
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'phrases#translator'
end
