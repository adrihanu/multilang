# Multilang

Simple application for translating phrases. It supports multiple languages.

Tags: `Ruby on Rails`, `Vue.js`

## Running
```bash
bundle install
bin/rails server
```


## Generate code from existing database
```bash
rails db:schema:dump
scaffold -c -p ./db/schema.rb
```

## Translations
```bash
pip3 install --user googletrans
pip3 install --user gTTS
```


# Database

<details>

```sql
CREATE TABLE `users` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    UNIQUE KEY (email)
) CHARSET=utf8mb4;


CREATE TABLE `languages` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    iso6391 VARCHAR(2) NOT NULL,
    iso6393 VARCHAR(3) NOT NULL,
    name VARCHAR(64) NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    UNIQUE KEY (iso6393)
) CHARSET=utf8mb4;


CREATE TABLE `phrases` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    content VARCHAR(255) NOT NULL,
    meaning VARCHAR(255) NOT NULL,
    verified BOOLEAN NOT NULL DEFAULT FALSE,
    language_id INT NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY language_key (language_id) REFERENCES `languages`(id),
    UNIQUE KEY (content, meaning, language_id)
) CHARSET=utf8mb4;

-- Grammatical cases of phrases 
CREATE TABLE `phrases_cases` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    phrase_id INT NOT NULL,
    content VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY phrase_key (phrase_id) REFERENCES `phrases`(id),
    UNIQUE KEY (content, phrase_id)
) CHARSET=utf8mb4;


CREATE TABLE `repetitions` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,

    positive INT NOT NULL DEFAULT 0,
    negative INT NOT NULL DEFAULT 0,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY user_key (user_id) REFERENCES `users`(id)
) CHARSET=utf8mb4;

CREATE TABLE `repetitions_phrases` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    phrase_id INT NOT NULL,
    repetition_id INT NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY phrase_key (phrase_id) REFERENCES `phrases`(id),
    FOREIGN KEY repetition_key (repetition_id) REFERENCES `repetitions`(id)
) CHARSET=utf8mb4;

CREATE TABLE `groups` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY user_key (user_id) REFERENCES `users`(id)
) CHARSET=utf8mb4;


CREATE TABLE `groups_subgroups` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    group_id INT NOT NULL,
    subgroup_id INT NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY group_key (group_id) REFERENCES `groups`(id),
    FOREIGN KEY subgroup_key (subgroup_id) REFERENCES `groups`(id),
    UNIQUE(subgroup_id, group_id)
) CHARSET=utf8mb4;


CREATE TABLE `groups_trainees` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    group_id INT NOT NULL,
    trainee_id INT NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY group_key (group_id) REFERENCES `groups`(id),
    FOREIGN KEY trainee_key (trainee_id) REFERENCES `users`(id),
    UNIQUE(trainee_id, group_id)
) CHARSET=utf8mb4;


CREATE TABLE `groups_repetitions` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    group_id INT NOT NULL,
    repetition_id INT NOT NULL,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY group_key (group_id) REFERENCES `groups`(id),
    FOREIGN KEY repetition_key (repetition_id) REFERENCES `repetitions`(id),
    UNIQUE(repetition_id, group_id)
) CHARSET=utf8mb4;


CREATE TABLE `users_languages`(
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    language_id INT NOT NULL,
    primary_language BOOLEAN NOT NULL DEFAULT FALSE,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,

    FOREIGN KEY user_key (user_id) REFERENCES `users`(id),
    FOREIGN KEY language_key (language_id) REFERENCES `languages`(id),
    UNIQUE(user_id, language_id, primary_language)
) CHARSET=utf8mb4;


INSERT INTO `users` (email, password, created_at, updated_at) VALUES ('adrihanu@gmail.com', 'a', NOW(), NOW());
INSERT INTO `languages` (iso6391, iso6393, name, created_at, updated_at) VALUES ('pl', 'pol', 'Polish', NOW(), NOW());
INSERT INTO `languages` (iso6391, iso6393, name, created_at, updated_at) VALUES ('en', 'eng','English', NOW(), NOW());
INSERT INTO `languages` (iso6391, iso6393, name, created_at, updated_at) VALUES ('fr', 'fra','French', NOW(), NOW());
```
</details>


