import re, time
from lxml import etree
from pprint import pprint
pathWikiXML = '../plwiktionary-20190101-pages-articles-multistream.xml'
#pathWikiXML = '../plwiktionary-20190120-pages-articles.xml'
#pathWikiXML = '../plwiktionary-20190120-pages-meta-current.xml'

class Page():
    def __init__(self):
        pass


page = None
for event, element in etree.iterparse(pathWikiXML, events=('start',)):
    tag_name = re.sub(r"\{.*\}", "", element.tag)
    if tag_name == 'page': 
        if page and page.ns == '0' and page.text:
            print('\n',page.title)

            templates = {}

            from mediawiki_parser.preprocessor import make_parser
            preprocessor = make_parser(templates)

            from mediawiki_parser.text import make_parser
            parser = make_parser()

            preprocessed_text = preprocessor.parse(page.text)
            output = parser.parse(preprocessed_text.leaves())
            print(output)

            for line in page.text.split('\n'):
                #print(line)
                if line[:3] == '== ':
                    print(line)
            #pprint(page.__dict__)

        page = Page()
    elif page:
        setattr(page, tag_name, element.text)

        #print(etree.tostring(element, method='xml'))
