require "application_system_test_case"

class GroupsSubgroupsTest < ApplicationSystemTestCase
  setup do
    @groups_subgroup = groups_subgroups(:one)
  end

  test "visiting the index" do
    visit groups_subgroups_url
    assert_selector "h1", text: "Groups Subgroups"
  end

  test "creating a Groups subgroup" do
    visit groups_subgroups_url
    click_on "New Groups Subgroup"

    fill_in "Group", with: @groups_subgroup.group_id
    fill_in "Subgroup", with: @groups_subgroup.subgroup_id
    click_on "Create Groups subgroup"

    assert_text "Groups subgroup was successfully created"
    click_on "Back"
  end

  test "updating a Groups subgroup" do
    visit groups_subgroups_url
    click_on "Edit", match: :first

    fill_in "Group", with: @groups_subgroup.group_id
    fill_in "Subgroup", with: @groups_subgroup.subgroup_id
    click_on "Update Groups subgroup"

    assert_text "Groups subgroup was successfully updated"
    click_on "Back"
  end

  test "destroying a Groups subgroup" do
    visit groups_subgroups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Groups subgroup was successfully destroyed"
  end
end
