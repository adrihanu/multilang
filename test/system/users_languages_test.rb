require "application_system_test_case"

class UsersLanguagesTest < ApplicationSystemTestCase
  setup do
    @users_language = users_languages(:one)
  end

  test "visiting the index" do
    visit users_languages_url
    assert_selector "h1", text: "Users Languages"
  end

  test "creating a Users language" do
    visit users_languages_url
    click_on "New Users Language"

    fill_in "Language", with: @users_language.language_id
    fill_in "Primary Language", with: @users_language.primary_language
    fill_in "User", with: @users_language.user_id
    click_on "Create Users language"

    assert_text "Users language was successfully created"
    click_on "Back"
  end

  test "updating a Users language" do
    visit users_languages_url
    click_on "Edit", match: :first

    fill_in "Language", with: @users_language.language_id
    fill_in "Primary Language", with: @users_language.primary_language
    fill_in "User", with: @users_language.user_id
    click_on "Update Users language"

    assert_text "Users language was successfully updated"
    click_on "Back"
  end

  test "destroying a Users language" do
    visit users_languages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Users language was successfully destroyed"
  end
end
