require "application_system_test_case"

class GroupsRepetitionsTest < ApplicationSystemTestCase
  setup do
    @groups_repetition = groups_repetitions(:one)
  end

  test "visiting the index" do
    visit groups_repetitions_url
    assert_selector "h1", text: "Groups Repetitions"
  end

  test "creating a Groups repetition" do
    visit groups_repetitions_url
    click_on "New Groups Repetition"

    fill_in "Group", with: @groups_repetition.group_id
    fill_in "Repetition", with: @groups_repetition.repetition_id
    click_on "Create Groups repetition"

    assert_text "Groups repetition was successfully created"
    click_on "Back"
  end

  test "updating a Groups repetition" do
    visit groups_repetitions_url
    click_on "Edit", match: :first

    fill_in "Group", with: @groups_repetition.group_id
    fill_in "Repetition", with: @groups_repetition.repetition_id
    click_on "Update Groups repetition"

    assert_text "Groups repetition was successfully updated"
    click_on "Back"
  end

  test "destroying a Groups repetition" do
    visit groups_repetitions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Groups repetition was successfully destroyed"
  end
end
