require "application_system_test_case"

class GroupsTraineesTest < ApplicationSystemTestCase
  setup do
    @groups_trainee = groups_trainees(:one)
  end

  test "visiting the index" do
    visit groups_trainees_url
    assert_selector "h1", text: "Groups Trainees"
  end

  test "creating a Groups trainee" do
    visit groups_trainees_url
    click_on "New Groups Trainee"

    fill_in "Group", with: @groups_trainee.group_id
    fill_in "Trainee", with: @groups_trainee.trainee_id
    click_on "Create Groups trainee"

    assert_text "Groups trainee was successfully created"
    click_on "Back"
  end

  test "updating a Groups trainee" do
    visit groups_trainees_url
    click_on "Edit", match: :first

    fill_in "Group", with: @groups_trainee.group_id
    fill_in "Trainee", with: @groups_trainee.trainee_id
    click_on "Update Groups trainee"

    assert_text "Groups trainee was successfully updated"
    click_on "Back"
  end

  test "destroying a Groups trainee" do
    visit groups_trainees_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Groups trainee was successfully destroyed"
  end
end
