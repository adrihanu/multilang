require "application_system_test_case"

class RepetitionsPhrasesTest < ApplicationSystemTestCase
  setup do
    @repetitions_phrase = repetitions_phrases(:one)
  end

  test "visiting the index" do
    visit repetitions_phrases_url
    assert_selector "h1", text: "Repetitions Phrases"
  end

  test "creating a Repetitions phrase" do
    visit repetitions_phrases_url
    click_on "New Repetitions Phrase"

    fill_in "Phrase", with: @repetitions_phrase.phrase_id
    fill_in "Repetition", with: @repetitions_phrase.repetition_id
    click_on "Create Repetitions phrase"

    assert_text "Repetitions phrase was successfully created"
    click_on "Back"
  end

  test "updating a Repetitions phrase" do
    visit repetitions_phrases_url
    click_on "Edit", match: :first

    fill_in "Phrase", with: @repetitions_phrase.phrase_id
    fill_in "Repetition", with: @repetitions_phrase.repetition_id
    click_on "Update Repetitions phrase"

    assert_text "Repetitions phrase was successfully updated"
    click_on "Back"
  end

  test "destroying a Repetitions phrase" do
    visit repetitions_phrases_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Repetitions phrase was successfully destroyed"
  end
end
