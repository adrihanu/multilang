require 'test_helper'

class RepetitionsPhrasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @repetitions_phrase = repetitions_phrases(:one)
  end

  test "should get index" do
    get repetitions_phrases_url
    assert_response :success
  end

  test "should get new" do
    get new_repetitions_phrase_url
    assert_response :success
  end

  test "should create repetitions_phrase" do
    assert_difference('RepetitionsPhrase.count') do
      post repetitions_phrases_url, params: { repetitions_phrase: { phrase_id: @repetitions_phrase.phrase_id, repetition_id: @repetitions_phrase.repetition_id } }
    end

    assert_redirected_to repetitions_phrase_url(RepetitionsPhrase.last)
  end

  test "should show repetitions_phrase" do
    get repetitions_phrase_url(@repetitions_phrase)
    assert_response :success
  end

  test "should get edit" do
    get edit_repetitions_phrase_url(@repetitions_phrase)
    assert_response :success
  end

  test "should update repetitions_phrase" do
    patch repetitions_phrase_url(@repetitions_phrase), params: { repetitions_phrase: { phrase_id: @repetitions_phrase.phrase_id, repetition_id: @repetitions_phrase.repetition_id } }
    assert_redirected_to repetitions_phrase_url(@repetitions_phrase)
  end

  test "should destroy repetitions_phrase" do
    assert_difference('RepetitionsPhrase.count', -1) do
      delete repetitions_phrase_url(@repetitions_phrase)
    end

    assert_redirected_to repetitions_phrases_url
  end
end
