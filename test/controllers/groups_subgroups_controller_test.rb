require 'test_helper'

class GroupsSubgroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @groups_subgroup = groups_subgroups(:one)
  end

  test "should get index" do
    get groups_subgroups_url
    assert_response :success
  end

  test "should get new" do
    get new_groups_subgroup_url
    assert_response :success
  end

  test "should create groups_subgroup" do
    assert_difference('GroupsSubgroup.count') do
      post groups_subgroups_url, params: { groups_subgroup: { group_id: @groups_subgroup.group_id, subgroup_id: @groups_subgroup.subgroup_id } }
    end

    assert_redirected_to groups_subgroup_url(GroupsSubgroup.last)
  end

  test "should show groups_subgroup" do
    get groups_subgroup_url(@groups_subgroup)
    assert_response :success
  end

  test "should get edit" do
    get edit_groups_subgroup_url(@groups_subgroup)
    assert_response :success
  end

  test "should update groups_subgroup" do
    patch groups_subgroup_url(@groups_subgroup), params: { groups_subgroup: { group_id: @groups_subgroup.group_id, subgroup_id: @groups_subgroup.subgroup_id } }
    assert_redirected_to groups_subgroup_url(@groups_subgroup)
  end

  test "should destroy groups_subgroup" do
    assert_difference('GroupsSubgroup.count', -1) do
      delete groups_subgroup_url(@groups_subgroup)
    end

    assert_redirected_to groups_subgroups_url
  end
end
