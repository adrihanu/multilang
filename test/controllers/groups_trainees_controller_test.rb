require 'test_helper'

class GroupsTraineesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @groups_trainee = groups_trainees(:one)
  end

  test "should get index" do
    get groups_trainees_url
    assert_response :success
  end

  test "should get new" do
    get new_groups_trainee_url
    assert_response :success
  end

  test "should create groups_trainee" do
    assert_difference('GroupsTrainee.count') do
      post groups_trainees_url, params: { groups_trainee: { group_id: @groups_trainee.group_id, trainee_id: @groups_trainee.trainee_id } }
    end

    assert_redirected_to groups_trainee_url(GroupsTrainee.last)
  end

  test "should show groups_trainee" do
    get groups_trainee_url(@groups_trainee)
    assert_response :success
  end

  test "should get edit" do
    get edit_groups_trainee_url(@groups_trainee)
    assert_response :success
  end

  test "should update groups_trainee" do
    patch groups_trainee_url(@groups_trainee), params: { groups_trainee: { group_id: @groups_trainee.group_id, trainee_id: @groups_trainee.trainee_id } }
    assert_redirected_to groups_trainee_url(@groups_trainee)
  end

  test "should destroy groups_trainee" do
    assert_difference('GroupsTrainee.count', -1) do
      delete groups_trainee_url(@groups_trainee)
    end

    assert_redirected_to groups_trainees_url
  end
end
