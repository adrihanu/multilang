require 'test_helper'

class GroupsRepetitionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @groups_repetition = groups_repetitions(:one)
  end

  test "should get index" do
    get groups_repetitions_url
    assert_response :success
  end

  test "should get new" do
    get new_groups_repetition_url
    assert_response :success
  end

  test "should create groups_repetition" do
    assert_difference('GroupsRepetition.count') do
      post groups_repetitions_url, params: { groups_repetition: { group_id: @groups_repetition.group_id, repetition_id: @groups_repetition.repetition_id } }
    end

    assert_redirected_to groups_repetition_url(GroupsRepetition.last)
  end

  test "should show groups_repetition" do
    get groups_repetition_url(@groups_repetition)
    assert_response :success
  end

  test "should get edit" do
    get edit_groups_repetition_url(@groups_repetition)
    assert_response :success
  end

  test "should update groups_repetition" do
    patch groups_repetition_url(@groups_repetition), params: { groups_repetition: { group_id: @groups_repetition.group_id, repetition_id: @groups_repetition.repetition_id } }
    assert_redirected_to groups_repetition_url(@groups_repetition)
  end

  test "should destroy groups_repetition" do
    assert_difference('GroupsRepetition.count', -1) do
      delete groups_repetition_url(@groups_repetition)
    end

    assert_redirected_to groups_repetitions_url
  end
end
