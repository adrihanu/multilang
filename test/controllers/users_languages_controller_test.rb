require 'test_helper'

class UsersLanguagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_language = users_languages(:one)
  end

  test "should get index" do
    get users_languages_url
    assert_response :success
  end

  test "should get new" do
    get new_users_language_url
    assert_response :success
  end

  test "should create users_language" do
    assert_difference('UsersLanguage.count') do
      post users_languages_url, params: { users_language: { language_id: @users_language.language_id, primary_language: @users_language.primary_language, user_id: @users_language.user_id } }
    end

    assert_redirected_to users_language_url(UsersLanguage.last)
  end

  test "should show users_language" do
    get users_language_url(@users_language)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_language_url(@users_language)
    assert_response :success
  end

  test "should update users_language" do
    patch users_language_url(@users_language), params: { users_language: { language_id: @users_language.language_id, primary_language: @users_language.primary_language, user_id: @users_language.user_id } }
    assert_redirected_to users_language_url(@users_language)
  end

  test "should destroy users_language" do
    assert_difference('UsersLanguage.count', -1) do
      delete users_language_url(@users_language)
    end

    assert_redirected_to users_languages_url
  end
end
