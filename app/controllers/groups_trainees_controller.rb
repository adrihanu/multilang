class GroupsTraineesController < ApplicationController
  before_action :set_groups_trainee, only: [:show, :edit, :update, :destroy]

  # GET /groups_trainees
  # GET /groups_trainees.json
  def index
    @groups_trainees = GroupsTrainee.all
  end

  # GET /groups_trainees/1
  # GET /groups_trainees/1.json
  def show
  end

  # GET /groups_trainees/new
  def new
    @groups_trainee = GroupsTrainee.new
  end

  # GET /groups_trainees/1/edit
  def edit
  end

  # POST /groups_trainees
  # POST /groups_trainees.json
  def create
    @groups_trainee = GroupsTrainee.new(groups_trainee_params)

    respond_to do |format|
      if @groups_trainee.save
        format.html { redirect_to @groups_trainee, notice: 'Groups trainee was successfully created.' }
        format.json { render :show, status: :created, location: @groups_trainee }
      else
        format.html { render :new }
        format.json { render json: @groups_trainee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups_trainees/1
  # PATCH/PUT /groups_trainees/1.json
  def update
    respond_to do |format|
      if @groups_trainee.update(groups_trainee_params)
        format.html { redirect_to @groups_trainee, notice: 'Groups trainee was successfully updated.' }
        format.json { render :show, status: :ok, location: @groups_trainee }
      else
        format.html { render :edit }
        format.json { render json: @groups_trainee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups_trainees/1
  # DELETE /groups_trainees/1.json
  def destroy
    @groups_trainee.destroy
    respond_to do |format|
      format.html { redirect_to groups_trainees_url, notice: 'Groups trainee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groups_trainee
      @groups_trainee = GroupsTrainee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groups_trainee_params
      params.require(:groups_trainee).permit(:group_id, :trainee_id)
    end
end
