class GroupsSubgroupsController < ApplicationController
  before_action :set_groups_subgroup, only: [:show, :edit, :update, :destroy]

  # GET /groups_subgroups
  # GET /groups_subgroups.json
  def index
    @groups_subgroups = GroupsSubgroup.all
  end

  # GET /groups_subgroups/1
  # GET /groups_subgroups/1.json
  def show
  end

  # GET /groups_subgroups/new
  def new
    @groups_subgroup = GroupsSubgroup.new
  end

  # GET /groups_subgroups/1/edit
  def edit
  end

  # POST /groups_subgroups
  # POST /groups_subgroups.json
  def create
    @groups_subgroup = GroupsSubgroup.new(groups_subgroup_params)

    respond_to do |format|
      if @groups_subgroup.save
        format.html { redirect_to @groups_subgroup, notice: 'Groups subgroup was successfully created.' }
        format.json { render :show, status: :created, location: @groups_subgroup }
      else
        format.html { render :new }
        format.json { render json: @groups_subgroup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups_subgroups/1
  # PATCH/PUT /groups_subgroups/1.json
  def update
    respond_to do |format|
      if @groups_subgroup.update(groups_subgroup_params)
        format.html { redirect_to @groups_subgroup, notice: 'Groups subgroup was successfully updated.' }
        format.json { render :show, status: :ok, location: @groups_subgroup }
      else
        format.html { render :edit }
        format.json { render json: @groups_subgroup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups_subgroups/1
  # DELETE /groups_subgroups/1.json
  def destroy
    @groups_subgroup.destroy
    respond_to do |format|
      format.html { redirect_to groups_subgroups_url, notice: 'Groups subgroup was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groups_subgroup
      @groups_subgroup = GroupsSubgroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groups_subgroup_params
      params.require(:groups_subgroup).permit(:group_id, :subgroup_id)
    end
end
