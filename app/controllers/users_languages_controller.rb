class UsersLanguagesController < ApplicationController
  before_action :set_users_language, only: [:show, :edit, :update, :destroy]

  # GET /users_languages
  # GET /users_languages.json
  def index
    @users_languages = UsersLanguage.all
  end

  # GET /users_languages/1
  # GET /users_languages/1.json
  def show
  end

  # GET /users_languages/new
  def new
    @users_language = UsersLanguage.new
  end

  # GET /users_languages/1/edit
  def edit
  end

  # POST /users_languages
  # POST /users_languages.json
  def create
    @users_language = UsersLanguage.new(users_language_params)

    respond_to do |format|
      if @users_language.save
        format.html { redirect_to @users_language, notice: 'Users language was successfully created.' }
        format.json { render :show, status: :created, location: @users_language }
      else
        format.html { render :new }
        format.json { render json: @users_language.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_languages/1
  # PATCH/PUT /users_languages/1.json
  def update
    respond_to do |format|
      if @users_language.update(users_language_params)
        format.html { redirect_to @users_language, notice: 'Users language was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_language }
      else
        format.html { render :edit }
        format.json { render json: @users_language.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_languages/1
  # DELETE /users_languages/1.json
  def destroy
    @users_language.destroy
    respond_to do |format|
      format.html { redirect_to users_languages_url, notice: 'Users language was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_language
      @users_language = UsersLanguage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_language_params
      params.require(:users_language).permit(:user_id, :language_id, :primary_language)
    end
end
