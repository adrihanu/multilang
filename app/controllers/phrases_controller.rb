class PhrasesController < ApplicationController
  before_action :set_phrase, only: [:show, :edit, :update, :destroy]

  # GET /phrases
  # GET /phrases.json
  def index
    @phrases = Phrase.all
  end

  # GET /phrases/translator
  def translator
    @phrase = Phrase.new
  end

  # GET /phrases/translate
  def translate
    @translations = []
    phrase = params[:phrase]
    sl = params[:sl]
    dl = params[:dl]
    ml = params[:ml]
    glosbe_translate(sl, dl, ml, phrase)
    render :json => @translations
  end

  # GET /phrases/1
  # GET /phrases/1.json
  def show
  end

  # GET /phrases/new
  def new
    @phrase = Phrase.new
  end

  # GET /phrases/1/edit
  def edit
  end

  # POST /phrases
  # POST /phrases.json
  def create
    @phrase = Phrase.new(phrase_params)

    respond_to do |format|
      if @phrase.save
        format.html { redirect_to @phrase, notice: 'Phrase was successfully created.' }
        format.json { render :show, status: :created, location: @phrase }
      else
        format.html { render :new }
        format.json { render json: @phrase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phrases/1
  # PATCH/PUT /phrases/1.json
  def update
    respond_to do |format|
      if @phrase.update(phrase_params)
        format.html { redirect_to @phrase, notice: 'Phrase was successfully updated.' }
        format.json { render :show, status: :ok, location: @phrase }
      else
        format.html { render :edit }
        format.json { render json: @phrase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phrases/1
  # DELETE /phrases/1.json
  def destroy
    @phrase.destroy
    respond_to do |format|
      format.html { redirect_to phrases_url, notice: 'Phrase was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phrase
      @phrase = Phrase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phrase_params
      params.require(:phrase).permit(:content, :meaning, :language_id)
    end
    # Translate using `glosbe` translator.
    # source language, destination language, meaning language, phrase to translate
    #
    def glosbe_translate(sl, dl, ml, phrase)
      url = "https://glosbe.com/gapi/translate/"
      glosbe_params = {:from=>sl, :dest=>dl, :format=>"json", :phrase=>phrase, :pretty=>"true"}
      glosbe_translations = HTTP.via("localhost", 8123).get(url, :params => glosbe_params).parse
      glosbe_translations["tuc"].each do |glosbe_translation|
        translation = {:phrase=>'', :meaning=>''}
        if !glosbe_translation["phrase"] 
          next
        end  
        translation[:phrase] = glosbe_translation["phrase"]["text"]
        if glosbe_translation["meanings"] 
          glosbe_translation["meanings"].each do |glosbe_meaning|
            if glosbe_meaning["language"] == ml
              translation[:meaning] = glosbe_meaning["text"]
              break
            end
          end
        end
        @translations.push(translation)
      end
    end
end
