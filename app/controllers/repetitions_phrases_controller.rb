class RepetitionsPhrasesController < ApplicationController
  before_action :set_repetitions_phrase, only: [:show, :edit, :update, :destroy]

  # GET /repetitions_phrases
  # GET /repetitions_phrases.json
  def index
    @repetitions_phrases = RepetitionsPhrase.all
  end

  # GET /repetitions_phrases/1
  # GET /repetitions_phrases/1.json
  def show
  end

  # GET /repetitions_phrases/new
  def new
    @repetitions_phrase = RepetitionsPhrase.new
  end

  # GET /repetitions_phrases/1/edit
  def edit
  end

  # POST /repetitions_phrases
  # POST /repetitions_phrases.json
  def create
    @repetitions_phrase = RepetitionsPhrase.new(repetitions_phrase_params)

    respond_to do |format|
      if @repetitions_phrase.save
        format.html { redirect_to @repetitions_phrase, notice: 'Repetitions phrase was successfully created.' }
        format.json { render :show, status: :created, location: @repetitions_phrase }
      else
        format.html { render :new }
        format.json { render json: @repetitions_phrase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /repetitions_phrases/1
  # PATCH/PUT /repetitions_phrases/1.json
  def update
    respond_to do |format|
      if @repetitions_phrase.update(repetitions_phrase_params)
        format.html { redirect_to @repetitions_phrase, notice: 'Repetitions phrase was successfully updated.' }
        format.json { render :show, status: :ok, location: @repetitions_phrase }
      else
        format.html { render :edit }
        format.json { render json: @repetitions_phrase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /repetitions_phrases/1
  # DELETE /repetitions_phrases/1.json
  def destroy
    @repetitions_phrase.destroy
    respond_to do |format|
      format.html { redirect_to repetitions_phrases_url, notice: 'Repetitions phrase was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_repetitions_phrase
      @repetitions_phrase = RepetitionsPhrase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def repetitions_phrase_params
      params.require(:repetitions_phrase).permit(:phrase_id, :repetition_id)
    end
end
