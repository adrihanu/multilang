class GroupsRepetitionsController < ApplicationController
  before_action :set_groups_repetition, only: [:show, :edit, :update, :destroy]

  # GET /groups_repetitions
  # GET /groups_repetitions.json
  def index
    @groups_repetitions = GroupsRepetition.all
  end

  # GET /groups_repetitions/1
  # GET /groups_repetitions/1.json
  def show
  end

  # GET /groups_repetitions/new
  def new
    @groups_repetition = GroupsRepetition.new
  end

  # GET /groups_repetitions/1/edit
  def edit
  end

  # POST /groups_repetitions
  # POST /groups_repetitions.json
  def create
    @groups_repetition = GroupsRepetition.new(groups_repetition_params)

    respond_to do |format|
      if @groups_repetition.save
        format.html { redirect_to @groups_repetition, notice: 'Groups repetition was successfully created.' }
        format.json { render :show, status: :created, location: @groups_repetition }
      else
        format.html { render :new }
        format.json { render json: @groups_repetition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups_repetitions/1
  # PATCH/PUT /groups_repetitions/1.json
  def update
    respond_to do |format|
      if @groups_repetition.update(groups_repetition_params)
        format.html { redirect_to @groups_repetition, notice: 'Groups repetition was successfully updated.' }
        format.json { render :show, status: :ok, location: @groups_repetition }
      else
        format.html { render :edit }
        format.json { render json: @groups_repetition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups_repetitions/1
  # DELETE /groups_repetitions/1.json
  def destroy
    @groups_repetition.destroy
    respond_to do |format|
      format.html { redirect_to groups_repetitions_url, notice: 'Groups repetition was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groups_repetition
      @groups_repetition = GroupsRepetition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groups_repetition_params
      params.require(:groups_repetition).permit(:group_id, :repetition_id)
    end
end
