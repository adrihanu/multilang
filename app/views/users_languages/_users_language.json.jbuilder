json.extract! users_language, :id, :user_id, :language_id, :primary_language, :created_at, :updated_at
json.url users_language_url(users_language, format: :json)
