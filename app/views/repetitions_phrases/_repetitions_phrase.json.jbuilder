json.extract! repetitions_phrase, :id, :phrase_id, :repetition_id, :created_at, :updated_at
json.url repetitions_phrase_url(repetitions_phrase, format: :json)
