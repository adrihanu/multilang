json.extract! phrase, :id, :content, :meaning, :language_id, :created_at, :updated_at
json.url phrase_url(phrase, format: :json)
