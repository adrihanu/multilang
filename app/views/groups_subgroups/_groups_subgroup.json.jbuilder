json.extract! groups_subgroup, :id, :group_id, :subgroup_id, :created_at, :updated_at
json.url groups_subgroup_url(groups_subgroup, format: :json)
