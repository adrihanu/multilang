json.extract! language, :id, :iso6391, :iso6393, :name, :created_at, :updated_at
json.url language_url(language, format: :json)
