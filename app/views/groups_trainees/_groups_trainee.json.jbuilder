json.extract! groups_trainee, :id, :group_id, :trainee_id, :created_at, :updated_at
json.url groups_trainee_url(groups_trainee, format: :json)
