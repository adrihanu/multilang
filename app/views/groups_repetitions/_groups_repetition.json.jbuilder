json.extract! groups_repetition, :id, :group_id, :repetition_id, :created_at, :updated_at
json.url groups_repetition_url(groups_repetition, format: :json)
