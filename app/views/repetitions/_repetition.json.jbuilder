json.extract! repetition, :id, :user_id, :positive, :negative, :created_at, :updated_at
json.url repetition_url(repetition, format: :json)
