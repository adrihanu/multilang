class GroupsSubgroup < ApplicationRecord
  belongs_to :group
  belongs_to :subgroup
end
