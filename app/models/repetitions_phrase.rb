class RepetitionsPhrase < ApplicationRecord
  belongs_to :phrase
  belongs_to :repetition
end
