# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "groups", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "name", null: false
    t.string "description", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], name: "user_key"
  end

  create_table "groups_repetitions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "group_id", null: false
    t.integer "repetition_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["group_id"], name: "group_key"
    t.index ["repetition_id", "group_id"], name: "repetition_id", unique: true
  end

  create_table "groups_subgroups", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "group_id", null: false
    t.integer "subgroup_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["group_id"], name: "group_key"
    t.index ["subgroup_id", "group_id"], name: "subgroup_id", unique: true
  end

  create_table "groups_trainees", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "group_id", null: false
    t.integer "trainee_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["group_id"], name: "group_key"
    t.index ["trainee_id", "group_id"], name: "trainee_id", unique: true
  end

  create_table "languages", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "iso6391", limit: 2, null: false
    t.string "iso6393", limit: 3, null: false
    t.string "name", limit: 64, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["iso6393"], name: "iso6393", unique: true
  end

  create_table "phrases", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "content", null: false
    t.string "meaning", null: false
    t.integer "language_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["content", "meaning", "language_id"], name: "content", unique: true
    t.index ["language_id"], name: "language_key"
  end

  create_table "repetitions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "positive", default: 0, null: false
    t.integer "negative", default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], name: "user_key"
  end

  create_table "repetitions_phrases", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "phrase_id", null: false
    t.integer "repetition_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["phrase_id"], name: "phrase_key"
    t.index ["repetition_id"], name: "repetition_key"
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "email", null: false
    t.string "password", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["email"], name: "email", unique: true
  end

  create_table "users_languages", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "language_id", null: false
    t.boolean "primary_language", default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["language_id"], name: "language_key"
    t.index ["user_id", "language_id", "primary_language"], name: "user_id", unique: true
  end

  add_foreign_key "groups", "users", name: "groups_ibfk_1"
  add_foreign_key "groups_repetitions", "groups", name: "groups_repetitions_ibfk_1"
  add_foreign_key "groups_repetitions", "repetitions", name: "groups_repetitions_ibfk_2"
  add_foreign_key "groups_subgroups", "groups", column: "subgroup_id", name: "groups_subgroups_ibfk_2"
  add_foreign_key "groups_subgroups", "groups", name: "groups_subgroups_ibfk_1"
  add_foreign_key "groups_trainees", "groups", name: "groups_trainees_ibfk_1"
  add_foreign_key "groups_trainees", "users", column: "trainee_id", name: "groups_trainees_ibfk_2"
  add_foreign_key "phrases", "languages", name: "phrases_ibfk_1"
  add_foreign_key "repetitions", "users", name: "repetitions_ibfk_1"
  add_foreign_key "repetitions_phrases", "phrases", name: "repetitions_phrases_ibfk_1"
  add_foreign_key "repetitions_phrases", "repetitions", name: "repetitions_phrases_ibfk_2"
  add_foreign_key "users_languages", "languages", name: "users_languages_ibfk_2"
  add_foreign_key "users_languages", "users", name: "users_languages_ibfk_1"
end
